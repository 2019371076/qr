import { Link } from 'expo-router';
import { Button, Pressable, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function Page() {
    return (
        <View style={styles.main}>
            <Link href="/generar" asChild>
                <TouchableOpacity style={styles.linkButton}>
                    <Text style={styles.linkText}>Generar QRCode</Text>
                </TouchableOpacity>
            </Link>
            <Link href="/escanear" asChild>
                <TouchableOpacity style={styles.linkButton}>
                    <Text style={styles.linkText}>Escanear QRCode</Text>
                </TouchableOpacity>
            </Link>
        </View>
    );
}

const styles = StyleSheet.create({
    main: {
        height: '100%',
        justifyContent: 'space-evenly',
        padding: 20
    },
    linkButton: {
        backgroundColor: '#11a894',
        height: 60,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    linkText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white',
    }
})